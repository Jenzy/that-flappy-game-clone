﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ThatFlappyGameClone {
    public class Obstacles {
        Game1 game;
        Random random = new Random();
        Texture2D texObstacleBottom;
        Texture2D texObstacleTop;
        Queue<Obstacle> obstacles;
        int obstacleWidth;
        int gapHeight = 150;
        int horizontalspacing = 200;
        int marginTopBottom = 50;


        public Obstacles( Game1 game ) {
            this.game = game;
            obstacles = new Queue<Obstacle>();
        }

        public void Init( Texture2D bottom, Texture2D top ) {
            this.texObstacleBottom = bottom;
            this.texObstacleTop = top;
            this.obstacleWidth = Math.Max( bottom.Width, top.Width );

            Reset();
        }

        public void Update( GameTime gameTime ) {
            int dx = (int) (-game.Player.Velocity.X * gameTime.ElapsedGameTime.TotalSeconds);
            foreach( Obstacle o in obstacles )
                o.Move( dx, 0 );

            float playerX = game.Player.Position.X - obstacleWidth;
            foreach(Obstacle o in obstacles.Where( o => (o.X < playerX) && !o.Passed )){
                game.Player.ObstaclePassed();
                o.Passed = true;
            }

            if( obstacles.First().X < -obstacleWidth ) {
                obstacles.Dequeue();
                obstacles.Enqueue( new Obstacle( this, obstacles.Last().X + horizontalspacing + obstacleWidth ) );
            }
        }

        public void Draw( SpriteBatch spriteBatch ) {
            foreach( Obstacle obstacle in obstacles )
                obstacle.Draw( spriteBatch );
        }

        public bool CheckCollisions( Player player ) {
            return obstacles.Any( o => o.CollidesWithCircle( player.Position, player.Radius ) );
        }

        public void Reset() {
            obstacles.Clear();

            int space = horizontalspacing + obstacleWidth;
            for( int x = (int) (game.ScreenWidth * 0.75f); x <= 2 * game.ScreenWidth; x += space )
                obstacles.Enqueue( new Obstacle( this, x ) );
        }


        private class Obstacle {
            public int X { get; private set; }
            public int Y { get; private set; }
            public bool Passed { get; set; }
            Obstacles o;
            Vector2 originTop = new Vector2( 0, 1 );

            Rectangle RectangleTop { get { return new Rectangle( X, 0, o.obstacleWidth, Y - o.gapHeight / 2 ); } }
            Rectangle RectangleBottom { get { return new Rectangle( X, Y + o.gapHeight / 2, o.obstacleWidth, o.game.ScreenHeight ); } }

            public Obstacle( Obstacles obstacles, int x ) {
                this.o = obstacles;
                this.X = x;
                int tmp = o.marginTopBottom + o.gapHeight / 2;
                this.Y = o.random.Next( tmp, o.game.ScreenHeight - tmp );
            }

            public void Move( int dx, int dy ) {
                X += dx;
                Y += dy;
            }

            public void Draw( SpriteBatch spriteBatch ) {

                //Top obstacle
                spriteBatch.Draw( o.texObstacleTop, new Vector2( X, Y - o.gapHeight / 2 - o.texObstacleTop.Height ), Color.White );
                if( Game1.DrawBB )
                    PrimitiveDrawing.DrawRectangle( spriteBatch, RectangleTop );

                //Bottom obstacle
                spriteBatch.Draw( o.texObstacleBottom, new Vector2( X, Y + o.gapHeight / 2 ), Color.White );
                if( Game1.DrawBB )
                    PrimitiveDrawing.DrawRectangle( spriteBatch, RectangleBottom );
            }

            public bool CollidesWithCircle( Vector2 center, float radius ) {
                if( center.X + radius < X || center.X - radius > X + o.obstacleWidth )
                    return false;
                else {
                    Rectangle top = RectangleTop;
                    Rectangle bottom = RectangleBottom;
                    if( center.X >= X && center.X <= X + o.obstacleWidth ) {
                        if( center.Y - radius < top.Bottom || center.Y + radius > bottom.Top )
                            return true;
                    } else {
                        if( distance( center, new Vector2( top.Left, top.Bottom ) ) < radius ) return true;
                        if( distance( center, new Vector2( top.Right, top.Bottom ) ) < radius ) return true;
                        if( distance( center, new Vector2( bottom.Left, bottom.Top ) ) < radius ) return true;
                        if( distance( center, new Vector2( bottom.Right, bottom.Top ) ) < radius ) return true;
                    }
                }
                return false;
            }

            private float distance(Vector2 point1, Vector2 point2 ){
                return Math.Abs( (point1 - point2).Length() );
            }
        }


    }
}
