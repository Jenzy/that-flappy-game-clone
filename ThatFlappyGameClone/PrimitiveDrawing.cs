﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ThatFlappyGameClone {
    static public class PrimitiveDrawing {
        private static Texture2D whitePixel = null;

        private static void SetupWhitePixel(SpriteBatch batch) {
            if (whitePixel == null) {
                whitePixel = new Texture2D(batch.GraphicsDevice, 1, 1);
                whitePixel.SetData<Color>(new Color[] { Color.White });
            }
        }

        static public void DrawRectangle(SpriteBatch batch, Rectangle area, int width, Color color) {
            SetupWhitePixel(batch);
            batch.Draw(whitePixel, new Rectangle(area.X, area.Y, area.Width, width), color);
            batch.Draw(whitePixel, new Rectangle(area.X, area.Y, width, area.Height), color);
            batch.Draw(whitePixel, new Rectangle(area.X + area.Width - width, area.Y, width, area.Height), color);
            batch.Draw(whitePixel, new Rectangle(area.X, area.Y + area.Height - width, area.Width, width), color);
        }

        static public void DrawRectangle(SpriteBatch batch, Rectangle area) {
            DrawRectangle(batch, area, 1, Color.White);
        }

        public static void DrawCircle(SpriteBatch spritbatch, Vector2 center, float radius, Color color, int lineWidth = 1, int segments = 16) {
            Vector2[] vertex = new Vector2[segments];

            double increment = Math.PI * 2.0 / segments;
            double theta = 0.0;

            for (int i = 0; i < segments; i++) {
                vertex[i] = center + radius * new Vector2((float)Math.Cos(theta), (float)Math.Sin(theta));
                theta += increment;
            }

            DrawPolygon(spritbatch, vertex, segments, color, lineWidth);
        }

        public static void DrawCircle(SpriteBatch spritbatch, Vector2 center, float radius) {
            DrawCircle(spritbatch, center, radius, Color.White);
        }

        public static void DrawPolygon(SpriteBatch spriteBatch, Vector2[] vertex, int count, Color color, int lineWidth) {
            if (count > 0) {
                for (int i = 0; i < count - 1; i++) {
                    DrawLineSegment(spriteBatch, vertex[i], vertex[i + 1], color, lineWidth);
                }
                DrawLineSegment(spriteBatch, vertex[count - 1], vertex[0], color, lineWidth);
            }
        }
        public static void DrawLineSegment(SpriteBatch spriteBatch, Vector2 point1, Vector2 point2, Color color, int lineWidth=1) {
            SetupWhitePixel(spriteBatch);
            float angle = (float)Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            float length = Vector2.Distance(point1, point2);

            spriteBatch.Draw(whitePixel, point1, null, color, angle, Vector2.Zero, new Vector2(length, lineWidth),
            SpriteEffects.None, 0f);
        }

        public static void DrawLine(SpriteBatch spriteBatch, Vector2 point1, Vector2 point2) {
            DrawLineSegment(spriteBatch, point1, point2, Color.White, 1);
        }
    }
}
