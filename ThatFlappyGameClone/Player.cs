﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ThatFlappyGameClone {
    public class Player {
        private Vector2 mVelocity;
        private Vector2 mPosition;
        public Vector2 Position { get { return mPosition; } }
        public Vector2 Velocity { get { return mVelocity; } }
        public int Radius { get; private set; }
        public int Passes { get; private set; }

        private Texture2D texture;
        private Vector2 origin;
        private Vector2 startPosition;
        public static float g = 300;
        public static float jumpVelocity = -250;
        public static float scale = 0.5f;
        public static float velocityX = 150;

        public void Init(Texture2D texture, Vector2 position) {
            this.texture = texture;
            this.Radius = (int)(texture.Width / 2f * scale);
            this.mPosition = this.startPosition = position;
            this.origin = new Vector2(texture.Width / 2f, texture.Height / 2f);
            this.mVelocity = new Vector2(velocityX, 0);
            this.Passes = 0;
        }

        public void Update(GameTime time) {
            float dt = (float) time.ElapsedGameTime.TotalSeconds;
            mPosition.Y += mVelocity.Y * dt + 0.5f * g * dt * dt;
            mVelocity.Y += g * dt;
        }

        public void Jump() {
            mVelocity.Y = jumpVelocity;
        }

        public void Draw(SpriteBatch spriteBatch) {
            float rotation = (float) Math.Atan2(Velocity.Y, Velocity.X);
            spriteBatch.Draw(texture, Position, null, Color.White, rotation, origin, scale, SpriteEffects.None, 0f);
            if (Game1.DrawBB) {
                PrimitiveDrawing.DrawCircle(spriteBatch, Position, Radius);
                PrimitiveDrawing.DrawLine(spriteBatch, Position, Position + new Vector2((float)(Radius * Math.Cos(rotation)), (float)(Radius * Math.Sin(rotation))));
            }
        }

        public bool IsOutOfBounds( int width, int height ) {
            return Position.Y < 0 || Position.Y > height;// || Position.X < 0 || Position.X > width; 
        }

        public void ObstaclePassed() {
            Passes++;
        }

        public void Reset() {
            Passes = 0;
            mPosition = startPosition;
            mVelocity = new Vector2( velocityX, 0 );
        }
    }


}
