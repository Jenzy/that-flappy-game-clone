﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using System.IO;
#endregion

namespace ThatFlappyGameClone {
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game {
        public enum GameState { Start, Playing, GameOver }
        public GameState State { get; private set; }
        public static bool DrawBB { get; private set; }
        private const int defaultScreenWidth = 800;
        private const int defaultScreenHeight = 600;

        public int ScreenWidth { get { return graphics.GraphicsDevice.Viewport.Width; } }
        public int ScreenHeight { get { return graphics.GraphicsDevice.Viewport.Height; } }

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        KeyboardState currentKeyboardState;
        KeyboardState previousKeyboardState;
        Matrix spriteScale;

        public Player Player { get; private set; }
        Obstacles obstacles;
        FontRenderer fontRenderer;
        Texture2D texGameOver;

        public Game1()
            : base() {
            graphics = new GraphicsDeviceManager( this );
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize() {
            graphics.PreferredBackBufferHeight = defaultScreenHeight;
            graphics.PreferredBackBufferWidth = defaultScreenWidth;

            Player = new Player();
            obstacles = new Obstacles( this );
            fontRenderer = new FontRenderer();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent() {
            spriteBatch = new SpriteBatch( GraphicsDevice );
            float screenscale = (float) (ScreenWidth / (float) defaultScreenWidth);
            spriteScale = Matrix.CreateScale( screenscale, screenscale, 1 );

            Texture2D texPlayer = Content.Load<Texture2D>( "player" );
            Player.Init( texPlayer, new Vector2( ScreenWidth / 4, ScreenHeight / 2 ) );

            obstacles.Init( Content.Load<Texture2D>( "obstacleBottom" ), Content.Load<Texture2D>( "obstacleTop" ) );

            Texture2D fontTexture = Content.Load<Texture2D>( "font" );
            var fontFilePath = Path.Combine( Content.RootDirectory, "font.fnt" );
            fontRenderer.Init( fontTexture, fontFilePath );

            texGameOver = Content.Load<Texture2D>( "gameover" );

            State = GameState.Start;
        }


        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update( GameTime gameTime ) {
            HandleInput();

            if( State == GameState.Playing ) {
                obstacles.Update( gameTime );
                Player.Update( gameTime );

                CheckCollisions();
            }

            base.Update( gameTime );
        }

        private void CheckCollisions() {
            bool collision = obstacles.CheckCollisions( Player ) || Player.IsOutOfBounds( ScreenWidth, ScreenHeight );
            if( collision )
                State = GameState.GameOver;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw( GameTime gameTime ) {
            GraphicsDevice.Clear( Color.CornflowerBlue );

            spriteBatch.Begin( SpriteSortMode.Deferred, null, null, null, null, null, spriteScale );

            Player.Draw( spriteBatch );
            obstacles.Draw( spriteBatch );
            fontRenderer.DrawText( spriteBatch, 5, 5, "Score: " + Player.Passes );

            if( State == GameState.GameOver ) {
                Vector2 center = new Vector2( ScreenWidth / 2, ScreenHeight / 2 );
                spriteBatch.Draw( texGameOver, center, null, Color.White, 0f,
                    new Vector2( texGameOver.Width / 2, texGameOver.Height / 2 ), 1f, SpriteEffects.None, 0f );
                center.Y += 50;
            } else if( State == GameState.Start ) {
                fontRenderer.DrawText( spriteBatch, 5, 30, "Press Space to begin" );

            }

            spriteBatch.End();

            base.Draw( gameTime );
        }

        private void HandleInput() {
            previousKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();

            if( currentKeyboardState.IsKeyDown( Keys.Escape ) )
                Exit();
            if( KeyPressed( Keys.Space ) )
                switch( State ) {
                    case GameState.Start:
                        State = GameState.Playing;
                        break;
                    case GameState.Playing:
                        Player.Jump();
                        break;
                    case GameState.GameOver:
                        Reset();
                        break;
                }
            if( KeyPressed( Keys.D ) )
                DrawBB = !DrawBB;
        }

        private bool KeyPressed( Keys key ) {
            return previousKeyboardState.IsKeyUp( key ) && currentKeyboardState.IsKeyDown( key );
        }

        private void Reset() {
            Player.Reset();
            obstacles.Reset();
            State = GameState.Start;

        }
    }
}
