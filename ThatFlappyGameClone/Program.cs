﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
#endregion

namespace ThatFlappyGameClone {
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main( string[] args ) {
            for( int i=0; i < args.Length; i++ ) {
                if( args[i].StartsWith( "-" ) ) {
                    switch( args[i] ) {
                        case "-scale":
                            Player.scale = float.Parse( args[++i] );
                            break;
                        case "-g":
                            Player.g = float.Parse( args[++i] );
                            break;
                        case "-jump":
                            Player.jumpVelocity = float.Parse( args[++i] );
                            break;
                        case "-v":
                            Player.velocityX = float.Parse( args[++i] );
                            break;
                        default:
                            break;
                    }
                }
            }

            using( var game = new Game1() )
                game.Run();
        }
    }
#endif
}
