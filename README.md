That Flappy Game Clone
======================
![Screenshot](screen.png)

I wanted to try out Monogame, so I made this. A rip-off of that Flappy Bird game which ripped-off that [Helicopter Game](http://www.helicoptergame.net/)    

---
Adjust the physics, because I can't be bothered to find optimal values.  
Arguments:   
* -g <float>    	- gravity    
* -scale <float>    - player scale    
* -jump <float>     - jump velocity, should be < 0   
* -v <float>    	- horizontal velocity    

---

[Download](https://bitbucket.org/zero-slo/that-flappy-game-clone/downloads/ThatFlappyGameClone-win.zip)    
[Monogame](http://www.monogame.net/)    
 
---
[Text Rendering](http://www.craftworkgames.com/blog/tutorial-bmfont-rendering-with-monogame/)   
Because I couldn't be bothered to install XNA Game studio to compile fonts.